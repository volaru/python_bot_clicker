from PIL import ImageGrab
from fs.memoryfs import *
from fs import open_fs, opener
from fs.mountfs import MountFS

def make_fs():
    fs = MountFS()
    mem_fs = MemoryFS()
    fs.mount("/", mem_fs)
    return fs

filesystem = make_fs()

mem_fs = MemoryFS()
mem_fs.makedir("/test")
mem_fs.makedir("/test/tes2")
mem_fs.makedir("/test/tes3")
#print(mem_fs.tree())

mem_fs.makedir("/test/tes2/1")
mem_fs.makedir("/test/tes2/2")


a_list = [475, 294, 495, 314]
imTEST = ImageGrab.grab(bbox=a_list)  # X1,Y1,X2,Y2


combined_fs = MountFS(auto_close=False)
combined_fs.mount('MOUNTEDFS', mem_fs)

print(combined_fs.tree())
with opener.open_fs(filesystem, writeable=True):
    imTEST.save("im_temp.bmp")

print(filesystem.tree())



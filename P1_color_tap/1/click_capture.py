import time
import pyautogui
import mouse
import keyboard

def write_new_value(filename, key, new_value):
     #open file and reads all the lines
     f = open(filename, "r")
     lines = f.readlines()
     f.close()


     for i, line in enumerate(lines):
          #splits the line by '=' character
          #then it strips the lines of any "\n" character and " " (space)
          #and if that matches the key it will replace with new value

          if line.split('=')[0].strip(' \n') == key:
               lines[i] = key + ' = ' + new_value + '\n'

     f = open(filename, "w")
     f.write("".join(lines))
     f.close()

def click_capture():
     b_list = []
     while True:
          currentMouseX = None
          currentMouseY = None
          Xx, Yy = pyautogui.position()
          print(Xx, Yy)
          x = mouse.is_pressed(button='right')
          if x == True:
               currentMouseX, currentMouseY = pyautogui.position()
               mouse.release(button='right')

          if currentMouseX or currentMouseY is not None:
               b_list.append((currentMouseX, currentMouseY))
               currentMouseX = None
               currentMouseY = None

          print(b_list)

          try:
               if keyboard.is_pressed('ESC'):
                    print('Done capturing!')
                    break
               else:
                    pass
          except:
               break

     return(b_list)


positions = click_capture()

fullStr = str("[" + ', '.join(str(x) for x in positions) + "]")

write_new_value("config.py", 'cast_spell_order', fullStr)



import cv2
import numpy as np
import time
"""

FOR THIS TO WORK I MUST ALWAYS KNOW THE lengtht OF THE LIST

in this case len(x)

in the case of the image comapre len(x) is always 4 - X1, Y1, X2, Y2


Need to split this into 3 separate functions:
- one that writes the values into the image and creates the concatenated image
- one that reads the image and extracts the values
- one that recovers the original image to be used somewhere else

Note:
- No need to write the image without the values embeded as long as i can use it form memory



changes to other files:

- call embed_list into the new capture stats
   - testt it using the other 2 functions
- call rebuild_list into battle check to get the location of screenshot
   - test it using print adn saving image
- call rebuild_ogin to be used into image compare as array without writing a file
   - te



"""
start_time = time.time()


def onelist(n):
   listofzeros = [1] * n
   return listofzeros


def build_list(original_list):
   arr_max = 255
   final_list = []
   original_coordinates = original_list
   for i in range(0, len(original_coordinates)):
      final_list.append(original_coordinates[i] % arr_max)
      print(final_list)
      final_list.append(int(original_coordinates[i]/arr_max))
      print(final_list)
   return(final_list)


def rebuild_coordinates(x_list):
   final_list = x_list
   print(len(final_list))
   arr_max = 255
   i = 0
   j = i
   final_coord = []
   while i < int(len(final_list)):
      j = i + 1
      final_coord.append((final_list[i] + arr_max*final_list[j]))
      i = i+2
   return(final_coord)


def embed_list(a, im1, filename="im2_from_im1.bmp"):
   x = build_list(a)
   x1 = np.asarray(x)
   x_im1, y_im1, z_im1 = im1.shape
   im1_reshape = np.reshape(im1, (1, x_im1*y_im1*z_im1))
   array_dimension = (x_im1+1)*y_im1*z_im1 - x_im1*y_im1*z_im1

   zero_list = onelist(array_dimension-len(x))
   list_appended = zero_list + x

   array_from_list = np.zeros((1, len(list_appended)))
   array_from_list[0] = list_appended

   array_from_list_reshape = np.reshape(array_from_list, (1, y_im1, z_im1))

   im1_reshape_back = np.reshape(im1_reshape, (x_im1, y_im1, z_im1))

   im1_concat = np.concatenate((im1, array_from_list_reshape))
   im_write = cv2.imwrite(filename, im1_concat)
   im_write = cv2.imread(filename)
   return(im1_concat)


def rebuild_ogin(im1_concat):
   x_im1, y_im1, z_im1 = im1_concat.shape
   reshape_original_size = np.resize(im1_concat, (x_im1-1, y_im1, z_im1))
   #im_og = cv2.imwrite('og_size2.bmp',reshape_original_size)
   #im_og = cv2.imread('og_size2.bmp')
   return(reshape_original_size)


def rebuild_list(im1_concat, x):
   # Here i start making the list:
   x_imC, y_imC, z_imC = im1_concat.shape
   make_list = []

   if x % 3 == 0:
      for xw in range(0, int(x/3)):
            for yw in range(0, 3):
               make_list.append(im1_concat.item(
                  x_imC-1, y_imC-(int(x/3))+xw, yw))
   else:
      if x/3 - int(x/3) < 0.5:
            make_list.append(im1_concat.item(x_imC-1, y_imC-1-(int(x/3)), 2))
      elif x/3 - int(x/3) > 0.5:
            make_list.append(im1_concat.item(x_imC-1, y_imC-1-(int(x/3)), 1))
            make_list.append(im1_concat.item(x_imC-1, y_imC-1-(int(x/3)), 2))
      for xw in range(0, int(x/3)):
            for yw in range(0, 3):
               make_list.append(im1_concat.item(
                  x_imC-1, y_imC-(int(x/3))+xw, yw))

   returned_coordinates = rebuild_coordinates(make_list)
   return(returned_coordinates)

import pyautogui
import _thread
import time
import numpy as np
import PIL
from PIL import ImageGrab
from PIL import Image
import math, operator
from cursor_location_and_area import cursor_details
from image_comare import image_compare
import cv2

a = "Magic is happening here:"
print(a)
gamescreen = [0,0,403,256]
battlebutton_location = [343,207,403,256]
seconds_between_captures = 3
fixed_cursor_location = [380,237]

def compare_images_at_fixed_coordinates(location):
  while True:
    currentMouseX, currentMouseY = pyautogui.position()
    im1 = ImageGrab.grab(bbox=(location))  # X1,Y1,X2,Y2
    im1.save("im1.bmp")
    time.sleep(seconds_between_captures)
    im2 = ImageGrab.grab(bbox=(location))  # X1,Y1,X2,Y2
    im2.save("im2.bmp")
    image_compare()

def compare_image_at_cursor_location():
  while True:
    location_area = cursor_details(2)
    print(location_area)
    im1 = ImageGrab.grab(bbox=(location_area))  # X1,Y1,X2,Y2
    im1.save("im1.bmp")
    time.sleep(seconds_between_captures)
    im2 = ImageGrab.grab(bbox=(location_area))  # X1,Y1,X2,Y2
    im2.save("im2.bmp")
    image_compare()


#compare_images_at_fixed_coordinates(battlebutton_location)
compare_image_at_cursor_location()

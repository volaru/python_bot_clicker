import cv2
import numpy as np
import time
from image_magic import *

from PIL import ImageGrab

def image_compare(image1, image2, zeroCounter=200):

 difference = cv2.subtract(image1, image2)
 non_zeros_count = np.count_nonzero(difference)
 cv2.imwrite("difference_first.bmp", difference)
 print("non zero count: ", non_zeros_count)
 print("Compared with 2: ", zeroCounter)


 if non_zeros_count < zeroCounter:
  difference = difference*0

 result = not np.any(difference) #if difference is all zeros it will return False

 if result is True:
  #cv2.imwrite("result_true.bmp", difference)
  return True
 else:
  #cv2.imwrite("result_false.bmp", difference)
  return False

import pyautogui


def cursor_details(loc_type, setW=10, setH=10, currentMouseX=10, currentMouseY=10):

   if loc_type == 1:
      screenWidth, screenHeight = pyautogui.size()
      X1 = screenWidth - (screenWidth - currentMouseX) - setW/2
      Y1 = screenHeight - (screenHeight - currentMouseY) - setH/2
      X2 = currentMouseX + setW/2
      Y2 = currentMouseY + setH/2
      location_area = [X1, Y1, X2, Y2]
      #print("Center of square: ",currentMouseX, currentMouseY)
      return [X1, Y1, X2, Y2]
   elif loc_type == 2:
      currentMouseX, currentMouseY = pyautogui.position()
      screenWidth, screenHeight = pyautogui.size()
      X1 = screenWidth - (screenWidth - currentMouseX) - setW/2
      Y1 = screenHeight - (screenHeight - currentMouseY) - setH/2
      X2 = currentMouseX + setW/2
      Y2 = currentMouseY + setH/2
      location_area = [X1, Y1, X2, Y2]
      #print("Center of square: ",currentMouseX, currentMouseY)
      return [X1, Y1, X2, Y2]


def recover_coordinates(given_list, square_width, square_height):
   x0_temp1 = given_list[0] + square_width/2
   y0_temp1 = given_list[1] + square_height/2
   x0_temp2 = given_list[2] - square_width/2
   y0_temp2 = given_list[3] - square_height/2

   if x0_temp1 == x0_temp2:
      if y0_temp1 == y0_temp2:
            return [x0_temp1, y0_temp1]

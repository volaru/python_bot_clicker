import pyautogui
import _thread
import time
import numpy as np
import PIL
import cv2
import math
import operator
from cursor_location_and_area import *
from im_compare_smart import *
from config import *
from PIL import ImageGrab
from PIL import Image
from image_magic import *

print(
'''
.##.....##....###.....######...####..######..####.####.####
.###...###...##.##...##....##...##..##....##.####.####.####
.####.####..##...##..##.........##..##.......####.####.####
.##.###.##.##.....##.##...####..##..##........##...##...##.
.##.....##.#########.##....##...##..##.....................
.##.....##.##.....##.##....##...##..##....##.####.####.####
.##.....##.##.....##..######...####..######..####.####.####
'''
)


def cast_spells():

    x = cast_spell_order
    im_start = cv2.imread('im2_from_im1.bmp')
    a_list = rebuild_list(im_start, 8)
    print(a_list)
    imBTL_statc = rebuild_ogin(im_start)

    for i in x:
        imTEST = ImageGrab.grab(bbox=a_list)  # X1,Y1,X2,Y2
        imTEST.save('im_temp.bmp')
        time.sleep(0.3)
        imTEST = cv2.imread('im_temp.bmp')
        y = []
        y = i
        #print("Print Y: ", y)
        cX = y[0]
        cY = y[1]
        pyautogui.moveTo(cX, cY)

        if image_compare(imBTL_statc, imTEST) is False:
            #print(image_compare(imBTL_statc, imTEST))
            pyautogui.click()
            print("Casting spells from: ", cX, cY)
            time.sleep(0.3)
        else:
            print("I stopped casting")
            #print(image_compare(imBTL_statc, imTEST))
            time.sleep(3)


def battle_check():

    counter = 0
    spells_casted = 0
    spells_notcasted = 0
    im_start = cv2.imread('im2_from_im1.bmp')
    a_list = rebuild_list(im_start, 8)
    print(a_list)
    imBTL_statc = rebuild_ogin(im_start)

    while True:
        time.sleep(3)
        imTEST = ImageGrab.grab(bbox=a_list)  # X1,Y1,X2,Y2
        imTEST.save("im_temp.bmp", "BMP")
        time.sleep(2)
        imTEST = cv2.imread('im_temp.bmp')

        #location_area = cursor_details(1)
        #print("asta e location_area", location_area)
        # im1 = ImageGrab.grab(bbox=(location_area))  # X1,Y1,X2,Y2
        # im1.save("im1.bmp")

        if image_compare(imBTL_statc, imTEST) is True:
            print("I'm not in battle... will start fighting soon")
            currentMouseX, currentMouseY = pyautogui.position()
            pyautogui.moveTo(recover_coordinates(a_list))
            pyautogui.click()
            print("Moving back to this locatrion:",
                currentMouseX, currentMouseY)
            pyautogui.moveTo(currentMouseX, currentMouseY)
            # print(counter)
        else:
            print("I'm in battle!!!")


            if counter % 2 == 0:
                cast_spells()
                counter = counter + 1
                spells_casted = spells_casted + 1
                print("Casting spells")

            else:
                counter = counter + 1
                spells_notcasted = spells_notcasted + 1
                print("I'm not casting any spells")



battle_check()
import cv2
import pyautogui
import time
import numpy as np
import cursor_location_and_area
import mouse
from image_magic import *
from config import *
from PIL import ImageGrab


def make_window_next_to_cursor(cX, cY):
    location_area = cursor_location_and_area.cursor_details(2)
    #print("coordonate imagine: ", location_area)
    im1 = ImageGrab.grab(bbox=(location_area))
    im1_np = np.array(im1)
    im1_resized = cv2.resize(im1_np, (square_height+30, square_width+30))
    # print(im1_resized)
    frame = cv2.cvtColor(im1_resized, cv2.COLOR_BGR2RGB)

    cv2.namedWindow("small_image", cv2.WINDOW_AUTOSIZE)
    cv2.resizeWindow("small_image", square_height+30, square_width+30)
    cv2.moveWindow("small_image", cX, cY)
    cv2.imshow("small_image", frame)
    cv2.waitKey(1)


while True:
    currentMouseX, currentMouseY = pyautogui.position()
    make_window_next_to_cursor(currentMouseX+20, currentMouseY+40)
    x = mouse.is_pressed(button='left')
    if x == True:
        make_window_next_to_cursor(currentMouseX+20, currentMouseY+60)
        location_area = cursor_location_and_area.cursor_details(2)
        print("smile for the picture")
        imTEST = ImageGrab.grab(bbox=(location_area))  # X1,Y1,X2,Y2
        print("Am facut poza aici:", location_area)
        imTEST.save("new_BTL.bmp")

        imTEST = cv2.imread('new_BTL.bmp')

        im_concat = embed_list(location_area, imTEST)
        make_list2 = rebuild_list(im_concat, len(location_area)*2)
        print("the awesome list: ", make_list2)
        break

cv2.destroyWindow("small_image")
